#include <LGPRS.h>
#include <LGPRSClient.h>
#include <LGPRSServer.h>
#include <LGPS.h>

#define URL    "things.ubidots.com"
#define TOKEN  "9tq0PasM47PPwhIQ9fsU81PIogK411"          // replace with your Ubidots token generated in your profile tab
#define VARID1 "57ed1c32762542303f018abe"                // create a variable in Ubidots and put its ID here (http://app.ubidots.com/ubi/datasources/)
#define MEASURE_TIME 2000

LGPRSClient client;
gpsSentenceInfoStruct info;
char buff[256];
char payloadGPS[500];

namespace precinto
{
  typedef struct GPS_struct
  {
    char time[30];
    double latitude;
    double longitude;
    double altitude;
    int fixQuality;
    int nSattelites;
  }GPS_struct;

  static unsigned char getComma(unsigned char num,const char *str)
  {
    unsigned char i,j = 0;
    int len=strlen(str);
    for(i = 0;i < len;i ++)
    {
       if(str[i] == ',')
        j++;
       if(j == num)
        return i + 1; 
    }
    return 0; 
  }

  static double getDoubleNumber(const char *s)
  {
    char buf[10];
    unsigned char i;
    double rev;
    
    i=getComma(1, s);
    i = i - 1;
    strncpy(buf, s, i);
    buf[i] = 0;
    rev=atof(buf);
    return rev; 
  }

  static double getIntNumber(const char *s)
  {
    char buf[10];
    unsigned char i;
    double rev;
    
    i=getComma(1, s);
    i = i - 1;
    strncpy(buf, s, i);
    buf[i] = 0;
    rev=atoi(buf);
    return rev; 
  }

  void parseGPGGA(const char* GPGGAstr, GPS_struct *GPSdata)
  {
    /* Refer to http://www.gpsinformation.org/dale/nmea.htm#GGA
     * Sample data: $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
     * Where:
     *  GGA          Global Positioning System Fix Data
     *  123519       Fix taken at 12:35:19 UTC
     *  4807.038,N   Latitude 48 deg 07.038' N
     *  01131.000,E  Longitude 11 deg 31.000' E
     *  1            Fix quality: 0 = invalid
     *                            1 = GPS fix (SPS)
     *                            2 = DGPS fix
     *                            3 = PPS fix
     *                            4 = Real Time Kinematic
     *                            5 = Float RTK
     *                            6 = estimated (dead reckoning) (2.3 feature)
     *                            7 = Manual input mode
     *                            8 = Simulation mode
     *  08           Number of satellites being tracked
     *  0.9          Horizontal dilution of position
     *  545.4,M      Altitude, Meters, above mean sea level
     *  46.9,M       Height of geoid (mean sea level) above WGS84
     *                   ellipsoid
     *  (empty field) time in seconds since last DGPS update
     *  (empty field) DGPS station ID number
     *  *47          the checksum data, always begins with *
     */
    double latitude;
    double latitude_m;
    double longitude;
    double longitude_m;
    int sign;
    int tmp, hour, minute, second, num ;
    if(GPGGAstr[0] == '$')
    {
      tmp = getComma(1, GPGGAstr);
      hour     = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
      minute   = (GPGGAstr[tmp + 2] - '0') * 10 + (GPGGAstr[tmp + 3] - '0');
      second    = (GPGGAstr[tmp + 4] - '0') * 10 + (GPGGAstr[tmp + 5] - '0');
      
      sprintf(GPSdata->time, "UTC timer %2d-%2d-%2d", hour, minute, second);
      Serial.println(GPSdata->time);
      
      tmp = getComma(2, GPGGAstr);
      latitude = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
      latitude_m = getDoubleNumber(&GPGGAstr[tmp+2]);
      latitude += latitude_m/60;
      tmp = getComma(3, GPGGAstr);
      if(GPGGAstr[tmp] == 'S')
      {
        latitude *= -1;
      }
      GPSdata->latitude = latitude;

      tmp = getComma(4, GPGGAstr);
      longitude = (GPGGAstr[tmp + 0] - '0') * 100 + (GPGGAstr[tmp + 1] - '0') * 10 + (GPGGAstr[tmp + 2] - '0');
      longitude_m = getDoubleNumber(&GPGGAstr[tmp + 3]);
      longitude += longitude_m/60;
      tmp = getComma(5, GPGGAstr);
      if(GPGGAstr[tmp] == 'W')
      {
        longitude *= -1;
      }
      GPSdata->longitude = longitude;

      tmp = getComma(6, GPGGAstr);
      GPSdata->fixQuality = getIntNumber(&GPGGAstr[tmp]);

      sprintf(buff, "Coordinates = %10.4f, %10.4f", latitude, longitude);
      Serial.println(buff); 
      
      tmp = getComma(7, GPGGAstr);
      GPSdata->nSattelites = getIntNumber(&GPGGAstr[tmp]);    
      sprintf(buff, "satellites number = %d", GPSdata->nSattelites);
      Serial.println(buff); 
    }
    else
    {
      Serial.println("Not get data");
      GPSdata->fixQuality = 0; 
    }
  }

  void GPSpayload(char * payload, GPS_struct GPSdata)
  {
    sprintf(payload, "{\"value\":%d,\"context\":{ \"lat\":%10.4f,\"lng\":%10.4f}}", GPSdata.fixQuality, GPSdata.latitude, GPSdata.longitude) ;
  }

  int sendToUbidots(char* var_id, char* payload)
  {
    String payloadS = String(payload);
    String le = String(payloadS.length()); 
    LGPRSClient c;
    if (c.connect(URL, 80))
    {
      Serial.println("Connected to Ubidots");
      String response = "";

      // Build HTTP POST request
      c.print(F("POST /api/v1.6/variables/"));
      c.print(var_id);
      c.println(F("/values HTTP/1.1"));
      c.println(F("User-Agent: LinKit One/1.0"));
      c.print(F("X-Auth-Token: "));
      c.println(TOKEN);
      c.println(F("Connection: close"));
      c.println(F("Content-Type: application/json"));
      c.print(F("Content-Length: "));
      c.println(le);
      c.print(F("Host: "));
      c.println(URL);
      c.println(); 
      c.println(payload);
      c.println(); 

      int v;
      while(c.available())
      {  
        v = c.read();
        if(v < 0){
          Serial.println("No response.");
          break;
        }
        Serial.println((char)v);
      }
    }
    else
    {
      Serial.println("Couldn't connect to Ubidots");
      return -1;
    }
    return 1;
  }
}

precinto::GPS_struct GPS_data;

void setup()
{
  // Init LED
  pinMode(13, OUTPUT);
  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  Serial.println("setup()");
  Serial.println("Conecting to Virgin Mobile");

  // attempt to connect to GPRS network:
  while(!LGPRS.attachGPRS("web.vmc.net.co", "Virgin Mobile", NULL ))
  {
    delay(1000);
    Serial.println("retrying...");
  }
  Serial.println("Connected to Virgin Mobile Network");

  //Init GPS
  LGPS.powerOn();
  Serial.println("LGPS Power on, and waiting ..."); 

  delay(10000);

  Serial.println("\nStarting connection to server...");
  

  Serial.println("setup() done");
}

void loop()
{
  LGPS.getData(&info);
  precinto::parseGPGGA((const char*)info.GPGGA, &GPS_data);
  //if(GPS_data.fixQuality)
  {
    Serial.println("Sending Data");
    precinto::GPSpayload(payloadGPS,GPS_data);
    precinto::sendToUbidots(VARID1,payloadGPS);
  }
  // else
  // {
  //   Serial.println("No data from sattelites yet");
  // }
  delay(MEASURE_TIME);
}














----------------------------------------------------------------------------------------------------------------------------------------------------------
#include <LGPS.h>

gpsSentenceInfoStruct info;
char buff[256];

static unsigned char getComma(unsigned char num,const char *str)
{
  unsigned char i,j = 0;
  int len=strlen(str);
  for(i = 0;i < len;i ++)
  {
     if(str[i] == ',')
      j++;
     if(j == num)
      return i + 1; 
  }
  return 0; 
}

static double getDoubleNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atof(buf);
  return rev; 
}

static double getIntNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atoi(buf);
  return rev; 
}

typedef struct GPS_struct
{
  char time[30];
  float latitude;
  float longitude;
  float altitude;
  int fixQuality;
  int nSattelites;
} GPS_struct;

void parseGPGGA(const char* GPGGAstr, GPS_struct *GPSdata;)
{
  /* Refer to http://www.gpsinformation.org/dale/nmea.htm#GGA
   * Sample data: $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
   * Where:
   *  GGA          Global Positioning System Fix Data
   *  123519       Fix taken at 12:35:19 UTC
   *  4807.038,N   Latitude 48 deg 07.038' N
   *  01131.000,E  Longitude 11 deg 31.000' E
   *  1            Fix quality: 0 = invalid
   *                            1 = GPS fix (SPS)
   *                            2 = DGPS fix
   *                            3 = PPS fix
   *                            4 = Real Time Kinematic
   *                            5 = Float RTK
   *                            6 = estimated (dead reckoning) (2.3 feature)
   *                            7 = Manual input mode
   *                            8 = Simulation mode
   *  08           Number of satellites being tracked
   *  0.9          Horizontal dilution of position
   *  545.4,M      Altitude, Meters, above mean sea level
   *  46.9,M       Height of geoid (mean sea level) above WGS84
   *                   ellipsoid
   *  (empty field) time in seconds since last DGPS update
   *  (empty field) DGPS station ID number
   *  *47          the checksum data, always begins with *
   */
  double latitude;
  double longitude;
  int tmp, hour, minute, second, num ;
  if(GPGGAstr[0] == '$')
  {
    tmp = getComma(1, GPGGAstr);
    hour     = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
    minute   = (GPGGAstr[tmp + 2] - '0') * 10 + (GPGGAstr[tmp + 3] - '0');
    second    = (GPGGAstr[tmp + 4] - '0') * 10 + (GPGGAstr[tmp + 5] - '0');
    
    sprintf(GPSdata->hour, "UTC timer %2d-%2d-%2d", hour, minute, second);
    Serial.println(GPSdata->time);
    
    tmp = getComma(2, GPGGAstr);
    latitude = getDoubleNumber(&GPGGAstr[tmp]);
    tmp = getComma(4, GPGGAstr);
    longitude = getDoubleNumber(&GPGGAstr[tmp]);
    sprintf(buff, "latitude = %10.4f, longitude = %10.4f", latitude, longitude);
    Serial.println(buff); 
    
    tmp = getComma(7, GPGGAstr);
    num = getIntNumber(&GPGGAstr[tmp]);    
    sprintf(buff, "satellites number = %d", num);
    Serial.println(buff); 
  }
  else
  {
    Serial.println("Not get data"); 
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  LGPS.powerOn();
  Serial.println("LGPS Power on, and waiting ..."); 
  delay(3000);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("LGPS loop"); 
  LGPS.getData(&info);
  Serial.println((char*)info.GPGGA); 
  parseGPGGA((const char*)info.GPGGA);
  delay(2000);
}













/*

 Udp NTP Client

 Get the time from a Network Time Protocol (NTP) time server
 Demonstrates use of UDP sendPacket and ReceivePacket
 For more on NTP time servers and the messages needed to communicate with them,
 see http://en.wikipedia.org/wiki/Network_Time_Protocol

 created 4 Sep 2010
 by Michael Margolis
 modified 9 Apr 2012
 by Tom Igoe
 Modified 20 Aug 2014
 by MediaTek Inc.

 This code is in the public domain.

 */
 
#include <LGPRS.h>
#include <LGPRSUdp.h>

unsigned int localPort = 2390;      // local port to listen for UDP packets

#define TIME_SERVER "time-c.nist.gov"  // a list of NTP servers: http://tf.nist.gov/tf-cgi/servers.cgi

const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message

byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets

// A LGPRSUDP instance to let us send and receive packets over UDP with LinkIt
LGPRSUDP Udp;

void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(115200);

  Serial.println("setup()");

  // attempt to connect to Wifi network:
  while(!LGPRS.attachGPRS("web.vmc.net.co", "Virgin Mobile", NULL ))
  {
    delay(1000);
    Serial.println("retry WiFi AP");
  }
  Serial.println("Connected to wifi");

  delay(10000);

  Serial.println("\nStarting connection to server...");
  while(!Udp.begin(localPort))
  {
    Serial.println("retry begin");
    delay(1000);
  }

  Serial.println("setup() done");
}

void loop()
{
  sendNTPpacket(); // send an NTP packet to a time server
  // wait to see if a reply is available
  delay(1000);
  Serial.println( Udp.parsePacket() );
  if ( Udp.parsePacket() ) {
    Serial.println("packet received");
    // We've received a packet, read the data from it
    memset(packetBuffer, 0xcd, NTP_PACKET_SIZE);
    Udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer
    for(int i = 0; i < NTP_PACKET_SIZE; ++i)
    {
      Serial.print(packetBuffer[i], HEX);
    }
    Serial.println();


    //the timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, esxtract the two words:
    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    unsigned long secsSince1900 = highWord << 16 | lowWord;
    Serial.print("Seconds since Jan 1 1900 = " );
    Serial.println(secsSince1900);

    // now convert NTP time into everyday time:
    Serial.print("Unix time = ");
    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
    const unsigned long seventyYears = 2208988800UL;
    // subtract seventy years:
    unsigned long epoch = secsSince1900 - seventyYears;
    // print Unix time:
    Serial.println(epoch);


    // print the hour, minute and second:
    Serial.print("The UTC time is ");       // UTC is the time at Greenwich Meridian (GMT)
    Serial.print((epoch  % 86400L) / 3600); // print the hour (86400 equals secs per day)
    Serial.print(':');
    if ( ((epoch % 3600) / 60) < 10 ) {
      // In the first 10 minutes of each hour, we'll want a leading '0'
      Serial.print('0');
    }
    Serial.print((epoch  % 3600) / 60); // print the minute (3600 equals secs per minute)
    Serial.print(':');
    if ( (epoch % 60) < 10 ) {
      // In the first 10 seconds of each minute, we'll want a leading '0'
      Serial.print('0');
    }
    Serial.println(epoch % 60); // print the second
  }
  // wait ten seconds before asking for the time again
  delay(10000);
}

// send an NTP request to the time server at the given address
unsigned long sendNTPpacket()
{
  Serial.println("sendNTPpacket");
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  //Serial.println("2");
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  //Serial.println("3");

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(TIME_SERVER, 123); //NTP requests are to port 123
  //Serial.println("4");
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  //Serial.println("5");
  Udp.endPacket();
  //Serial.println("6");
}




